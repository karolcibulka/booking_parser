<?php

class Hotel_model extends CI_Model
{

	public function getGrabHotels(){
		return $this->db->select('hch_DestinationID as destination_id,hch_Web as web,hch_Lang as lang')
			->from('tr_hotel_comparator_hotels')
			->where('hch_Active',1)
			->where('hch_Deleted',0)
			->get()
			->result_array();
	}

	public function getGrabHotel($id){
		return $this->db->select('hch_DestinationID as destination_id,hch_Web as web,hch_Lang as lang')
			->from('tr_hotel_comparator_hotels')
			->where('hch_Active',1)
			->where('hch_Deleted',0)
			->where('hch_ID',$id)
			->get()
			->row_array();
	}

	public function getHotelConnections($hotelID){
		$compares = $this->db->select('
				hch.hch_ID as id,
				hch.hch_Web as web,
				hch.hch_Name as name,
				hch.hch_DestinationID as destination_id,
				hch.hch_Lang as lang,
				hchr.hrc_ID as room_category_id,
				hrc.hcrt_ID as room_type_id,
				hchr_StandardBedCount as standart_bed_count
			')
			->from('tr_hotel_comparator_hotels_pairs as chp')
			->where('chp.hs_ID',$hotelID)
			->join('tr_hotel_comparator_hotels as hch','chp.hch_ID = hch.hch_ID','left')
			->join('tr_hotel_channels_rooms as hchr','hchr.hs_ID = chp.hs_ID','left')
			->join('tr_hotel_channels_rooms_partners as hchrp','hchrp.hchr_ID = hchr.hchr_ID','left')
			->join('tr_hotel_room_categories as hrc','hrc.hrc_ID = hchr.hrc_ID','left')
			->where('hrc.hcrt_ID !=',null)
			->where('hchrp.hchp_ID',1)
			->get()
			->result_array();

		$response = array();

		if(!empty($compares)){
			foreach($compares as $compare){
				$response[$compare['id']]['id'] = $compare['id'];
				$response[$compare['id']]['destination_id'] = $compare['destination_id'];
				$response[$compare['id']]['web'] = $compare['web'];
				$response[$compare['id']]['name'] = $compare['name'];
				$response[$compare['id']]['lang'] = $compare['lang'];
				$response[$compare['id']]['rooms'][$compare['room_type_id']] = $compare['room_type_id'];
			}

		}

		return $response;
	}
}
