# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.23)
# Database: kts
# Generation Time: 2019-03-15 08:40:21 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table controllers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `controllers`;

CREATE TABLE `controllers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `menu` varchar(30) DEFAULT NULL,
  `description` varchar(30) DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

LOCK TABLES `controllers` WRITE;
/*!40000 ALTER TABLE `controllers` DISABLE KEYS */;

INSERT INTO `controllers` (`id`, `name`, `menu`, `description`, `active`)
VALUES
	(1,'dashboard','controller','Dashboards',1),
	(2,'navigation','controller','Navigácia',1),
	(3,'auth','controller','Authorizácia',1),
	(4,'permission','controller','Právomoci',1);

/*!40000 ALTER TABLE `controllers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `name`, `description`)
VALUES
	(1,'admin','Administrator'),
	(2,'members','General User');

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table icons
# ------------------------------------------------------------

DROP TABLE IF EXISTS `icons`;

CREATE TABLE `icons` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(20) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1050 DEFAULT CHARSET=utf8mb4;

LOCK TABLES `icons` WRITE;
/*!40000 ALTER TABLE `icons` DISABLE KEYS */;

INSERT INTO `icons` (`id`, `value`, `name`)
VALUES
	(1,'home','home'),
	(2,'home2','home2'),
	(3,'home5','home5'),
	(10,'magazine','magazine'),
	(12,'pencil','pencil'),
	(16,'pencil6','pencil6'),
	(17,'pencil7','pencil7'),
	(18,'eraser','eraser'),
	(19,'eraser2','eraser2'),
	(20,'eraser3','eraser3'),
	(27,'blog','blog'),
	(28,'pen6','pen6'),
	(31,'color-sampler','color-sampler'),
	(48,'image4','image4'),
	(49,'image5','image5'),
	(50,'camera','camera'),
	(51,'shutter','shutter'),
	(52,'headphones','headphones'),
	(53,'headset','headset'),
	(54,'music','music'),
	(55,'album','album'),
	(56,'tape','tape'),
	(57,'piano','piano'),
	(58,'speakers','speakers'),
	(59,'play','play'),
	(60,'clapboard-play','clapboard-play'),
	(61,'clapboard','clapboard'),
	(62,'media','media'),
	(63,'presentation','presentation'),
	(64,'movie','movie'),
	(65,'film','film'),
	(66,'film2','film2'),
	(67,'film3','film3'),
	(68,'film4','film4'),
	(69,'video-camera','video-camera'),
	(70,'video-camera2','video-camera2'),
	(71,'video-camera-slash','video-camera-slash'),
	(72,'video-camera3','video-camera3'),
	(73,'dice','dice'),
	(74,'chess-king','chess-king'),
	(75,'chess-queen','chess-queen'),
	(76,'chess','chess'),
	(77,'megaphone','megaphone'),
	(78,'new','new'),
	(79,'connection','connection'),
	(80,'station','station'),
	(81,'satellite-dish2','satellite-dish2'),
	(82,'feed','feed'),
	(83,'mic2','mic2'),
	(84,'mic-off2','mic-off2'),
	(85,'book','book'),
	(86,'book2','book2'),
	(87,'book-play','book-play'),
	(88,'book3','book3'),
	(89,'bookmark','bookmark'),
	(90,'books','books'),
	(91,'archive','archive'),
	(92,'reading','reading'),
	(93,'library2','library2'),
	(94,'graduation2','graduation2'),
	(95,'file-text','file-text'),
	(96,'profile','profile'),
	(97,'file-empty','file-empty'),
	(98,'file-empty2','file-empty2'),
	(99,'files-empty','files-empty'),
	(100,'files-empty2','files-empty2'),
	(101,'file-plus','file-plus'),
	(102,'file-plus2','file-plus2'),
	(103,'file-minus','file-minus'),
	(104,'file-minus2','file-minus2'),
	(105,'file-download','file-download'),
	(106,'file-download2','file-download2'),
	(107,'file-upload','file-upload'),
	(108,'file-upload2','file-upload2'),
	(109,'file-check','file-check'),
	(110,'file-check2','file-check2'),
	(111,'file-eye','file-eye'),
	(112,'file-eye2','file-eye2'),
	(113,'file-text2','file-text2'),
	(114,'file-text3','file-text3'),
	(115,'file-picture','file-picture'),
	(116,'file-picture2','file-picture2'),
	(117,'file-music','file-music'),
	(118,'file-music2','file-music2'),
	(119,'file-play','file-play'),
	(120,'file-play2','file-play2'),
	(121,'file-video','file-video'),
	(122,'file-video2','file-video2'),
	(123,'copy','copy'),
	(124,'copy2','copy2'),
	(125,'file-zip','file-zip'),
	(126,'file-zip2','file-zip2'),
	(127,'file-xml','file-xml'),
	(128,'file-xml2','file-xml2'),
	(129,'file-css','file-css'),
	(130,'file-css2','file-css2'),
	(131,'file-presentation','file-presentation'),
	(132,'file-presentation2','file-presentation2'),
	(133,'file-stats','file-stats'),
	(134,'file-stats2','file-stats2'),
	(135,'file-locked','file-locked'),
	(136,'file-locked2','file-locked2'),
	(137,'file-spreadsheet','file-spreadsheet'),
	(138,'file-spreadsheet2','file-spreadsheet2'),
	(139,'copy3','copy3'),
	(140,'copy4','copy4'),
	(141,'paste','paste'),
	(142,'paste2','paste2'),
	(143,'paste3','paste3'),
	(144,'paste4','paste4'),
	(145,'stack','stack'),
	(146,'stack2','stack2'),
	(147,'stack3','stack3'),
	(148,'folder','folder'),
	(149,'folder-search','folder-search'),
	(150,'folder-download','folder-download'),
	(151,'folder-upload','folder-upload'),
	(152,'folder-plus','folder-plus'),
	(153,'folder-plus2','folder-plus2'),
	(154,'folder-minus','folder-minus'),
	(155,'folder-minus2','folder-minus2'),
	(156,'folder-check','folder-check'),
	(157,'folder-heart','folder-heart'),
	(158,'folder-remove','folder-remove'),
	(159,'folder2','folder2'),
	(160,'folder-open','folder-open'),
	(161,'folder3','folder3'),
	(162,'folder4','folder4'),
	(163,'folder-plus3','folder-plus3'),
	(164,'folder-minus3','folder-minus3'),
	(165,'folder-plus4','folder-plus4'),
	(166,'folder-minus4','folder-minus4'),
	(167,'folder-download2','folder-download2'),
	(168,'folder-upload2','folder-upload2'),
	(169,'folder-download3','folder-download3'),
	(170,'folder-upload3','folder-upload3'),
	(171,'folder5','folder5'),
	(172,'folder-open2','folder-open2'),
	(173,'folder6','folder6'),
	(174,'folder-open3','folder-open3'),
	(175,'certificate','certificate'),
	(176,'cc','cc'),
	(177,'price-tag','price-tag'),
	(178,'price-tag2','price-tag2'),
	(179,'price-tags','price-tags'),
	(180,'price-tag3','price-tag3'),
	(181,'price-tags2','price-tags2'),
	(182,'barcode2','barcode2'),
	(183,'qrcode','qrcode'),
	(184,'ticket','ticket'),
	(185,'theater','theater'),
	(186,'store','store'),
	(187,'store2','store2'),
	(188,'cart','cart'),
	(189,'cart2','cart2'),
	(190,'cart4','cart4'),
	(191,'cart5','cart5'),
	(192,'cart-add','cart-add'),
	(193,'cart-add2','cart-add2'),
	(194,'cart-remove','cart-remove'),
	(195,'basket','basket'),
	(196,'bag','bag'),
	(197,'percent','percent'),
	(198,'coins','coins'),
	(199,'coin-dollar','coin-dollar'),
	(200,'coin-euro','coin-euro'),
	(201,'coin-pound','coin-pound'),
	(202,'coin-yen','coin-yen'),
	(203,'piggy-bank','piggy-bank'),
	(204,'wallet','wallet'),
	(205,'cash','cash'),
	(206,'cash2','cash2'),
	(207,'cash3','cash3'),
	(208,'cash4','cash4'),
	(209,'credit-card','credit-card'),
	(210,'credit-card2','credit-card2'),
	(211,'calculator4','calculator4'),
	(212,'calculator2','calculator2'),
	(213,'calculator3','calculator3'),
	(214,'chip','chip'),
	(215,'lifebuoy','lifebuoy'),
	(245,'reset','reset'),
	(246,'history','history'),
	(247,'watch','watch'),
	(248,'watch2','watch2'),
	(249,'alarm','alarm'),
	(250,'alarm-add','alarm-add'),
	(251,'alarm-check','alarm-check'),
	(252,'alarm-cancel','alarm-cancel'),
	(253,'bell2','bell2'),
	(254,'bell3','bell3'),
	(255,'bell-plus','bell-plus'),
	(256,'bell-minus','bell-minus'),
	(257,'bell-check','bell-check'),
	(258,'bell-cross','bell-cross'),
	(259,'calendar','calendar'),
	(260,'calendar2','calendar2'),
	(261,'calendar3','calendar3'),
	(262,'calendar52','calendar52'),
	(263,'printer','printer'),
	(264,'printer2','printer2'),
	(265,'printer4','printer4'),
	(266,'shredder','shredder'),
	(267,'mouse','mouse'),
	(268,'mouse-left','mouse-left'),
	(269,'mouse-right','mouse-right'),
	(270,'keyboard','keyboard'),
	(271,'typewriter','typewriter'),
	(272,'display','display'),
	(273,'display4','display4'),
	(274,'laptop','laptop'),
	(275,'mobile','mobile'),
	(276,'mobile2','mobile2'),
	(277,'tablet','tablet'),
	(278,'mobile3','mobile3'),
	(279,'tv','tv'),
	(280,'radio','radio'),
	(281,'cabinet','cabinet'),
	(282,'drawer','drawer'),
	(283,'drawer2','drawer2'),
	(284,'drawer-out','drawer-out'),
	(285,'drawer-in','drawer-in'),
	(286,'drawer3','drawer3'),
	(287,'box','box'),
	(288,'box-add','box-add'),
	(289,'box-remove','box-remove'),
	(290,'download','download'),
	(291,'upload','upload'),
	(292,'floppy-disk','floppy-disk'),
	(293,'floppy-disks','floppy-disks'),
	(294,'usb-stick','usb-stick'),
	(295,'drive','drive'),
	(296,'server','server'),
	(297,'database','database'),
	(298,'database2','database2'),
	(299,'database4','database4'),
	(300,'database-menu','database-menu'),
	(301,'database-add','database-add'),
	(302,'database-remove','database-remove'),
	(303,'database-insert','database-insert'),
	(304,'database-export','database-export'),
	(305,'database-upload','database-upload'),
	(306,'database-refresh','database-refresh'),
	(307,'database-diff','database-diff'),
	(308,'database-edit2','database-edit2'),
	(309,'database-check','database-check'),
	(310,'database-arrow','database-arrow'),
	(311,'database-time2','database-time2'),
	(312,'undo','undo'),
	(313,'redo','redo'),
	(314,'rotate-ccw','rotate-ccw'),
	(315,'rotate-cw','rotate-cw'),
	(316,'rotate-ccw2','rotate-ccw2'),
	(317,'rotate-cw2','rotate-cw2'),
	(318,'rotate-ccw3','rotate-ccw3'),
	(319,'rotate-cw3','rotate-cw3'),
	(320,'flip-vertical2','flip-vertical2'),
	(321,'flip-horizontal2','flip-horizontal2'),
	(322,'flip-vertical3','flip-vertical3'),
	(323,'flip-vertical4','flip-vertical4'),
	(324,'angle','angle'),
	(325,'shear','shear'),
	(326,'align-left','align-left'),
	(327,'align-right','align-right'),
	(328,'align-top','align-top'),
	(329,'align-bottom','align-bottom'),
	(330,'undo2','undo2'),
	(331,'redo2','redo2'),
	(332,'forward','forward'),
	(333,'reply','reply'),
	(334,'reply-all','reply-all'),
	(335,'bubble','bubble'),
	(336,'bubbles','bubbles'),
	(337,'bubbles2','bubbles2'),
	(338,'bubble2','bubble2'),
	(339,'bubbles3','bubbles3'),
	(340,'bubbles4','bubbles4'),
	(341,'bubble-notification','bubble-notification'),
	(342,'bubbles5','bubbles5'),
	(343,'bubbles6','bubbles6'),
	(344,'bubble6','bubble6'),
	(345,'bubbles7','bubbles7'),
	(346,'bubble7','bubble7'),
	(347,'bubbles8','bubbles8'),
	(348,'bubble8','bubble8'),
	(349,'bubble-dots3','bubble-dots3'),
	(350,'bubble-lines3','bubble-lines3'),
	(351,'bubble9','bubble9'),
	(352,'bubble-dots4','bubble-dots4'),
	(353,'bubble-lines4','bubble-lines4'),
	(354,'bubbles9','bubbles9'),
	(355,'bubbles10','bubbles10'),
	(356,'user','user'),
	(357,'users','users'),
	(358,'user-plus','user-plus'),
	(359,'user-minus','user-minus'),
	(360,'user-cancel','user-cancel'),
	(361,'user-block','user-block'),
	(362,'user-lock','user-lock'),
	(363,'user-check','user-check'),
	(364,'users2','users2'),
	(365,'users4','users4'),
	(366,'user-tie','user-tie'),
	(367,'collaboration','collaboration'),
	(368,'vcard','vcard'),
	(369,'hat','hat'),
	(370,'bowtie','bowtie'),
	(371,'quotes-left','quotes-left'),
	(372,'quotes-right','quotes-right'),
	(373,'quotes-left2','quotes-left2'),
	(374,'quotes-right2','quotes-right2'),
	(375,'hour-glass','hour-glass'),
	(376,'hour-glass2','hour-glass2'),
	(377,'hour-glass3','hour-glass3'),
	(378,'spinner','spinner'),
	(379,'spinner2','spinner2'),
	(380,'spinner3','spinner3'),
	(381,'spinner4','spinner4'),
	(382,'spinner6','spinner6'),
	(383,'spinner9','spinner9'),
	(384,'spinner10','spinner10'),
	(385,'spinner11','spinner11'),
	(386,'microscope','microscope'),
	(387,'enlarge','enlarge'),
	(388,'shrink','shrink'),
	(389,'enlarge3','enlarge3'),
	(390,'shrink3','shrink3'),
	(391,'enlarge5','enlarge5'),
	(392,'shrink5','shrink5'),
	(393,'enlarge6','enlarge6'),
	(394,'shrink6','shrink6'),
	(395,'enlarge7','enlarge7'),
	(396,'shrink7','shrink7'),
	(397,'key','key'),
	(398,'lock','lock'),
	(399,'lock2','lock2'),
	(400,'lock4','lock4'),
	(401,'unlocked','unlocked'),
	(402,'lock5','lock5'),
	(403,'unlocked2','unlocked2'),
	(404,'safe','safe'),
	(405,'wrench','wrench'),
	(406,'wrench2','wrench2'),
	(407,'wrench3','wrench3'),
	(408,'equalizer','equalizer'),
	(409,'equalizer2','equalizer2'),
	(410,'equalizer3','equalizer3'),
	(411,'equalizer4','equalizer4'),
	(412,'cog','cog'),
	(413,'cogs','cogs'),
	(414,'cog2','cog2'),
	(415,'cog3','cog3'),
	(416,'cog4','cog4'),
	(417,'cog52','cog52'),
	(418,'cog6','cog6'),
	(419,'cog7','cog7'),
	(420,'hammer','hammer'),
	(421,'hammer-wrench','hammer-wrench'),
	(422,'magic-wand','magic-wand'),
	(423,'magic-wand2','magic-wand2'),
	(424,'pulse2','pulse2'),
	(425,'aid-kit','aid-kit'),
	(426,'bug2','bug2'),
	(427,'construction','construction'),
	(428,'traffic-cone','traffic-cone'),
	(429,'traffic-lights','traffic-lights'),
	(430,'pie-chart','pie-chart'),
	(431,'pie-chart2','pie-chart2'),
	(432,'pie-chart3','pie-chart3'),
	(433,'pie-chart4','pie-chart4'),
	(434,'pie-chart5','pie-chart5'),
	(435,'pie-chart6','pie-chart6'),
	(436,'pie-chart7','pie-chart7'),
	(437,'stats-dots','stats-dots'),
	(438,'stats-bars','stats-bars'),
	(439,'pie-chart8','pie-chart8'),
	(440,'stats-bars2','stats-bars2'),
	(441,'stats-bars3','stats-bars3'),
	(442,'stats-bars4','stats-bars4'),
	(443,'chart','chart'),
	(444,'stats-growth','stats-growth'),
	(445,'stats-decline','stats-decline'),
	(446,'stats-growth2','stats-growth2'),
	(447,'stats-decline2','stats-decline2'),
	(448,'stairs-up','stairs-up'),
	(449,'stairs-down','stairs-down'),
	(450,'stairs','stairs'),
	(451,'ladder','ladder'),
	(452,'rating','rating'),
	(453,'rating2','rating2'),
	(454,'rating3','rating3'),
	(455,'podium','podium'),
	(456,'stars','stars'),
	(457,'medal-star','medal-star'),
	(458,'medal','medal'),
	(459,'medal2','medal2'),
	(460,'medal-first','medal-first'),
	(461,'medal-second','medal-second'),
	(462,'medal-third','medal-third'),
	(463,'crown','crown'),
	(464,'trophy2','trophy2'),
	(465,'trophy3','trophy3'),
	(466,'diamond','diamond'),
	(467,'trophy4','trophy4'),
	(468,'gift','gift'),
	(469,'pipe','pipe'),
	(470,'mustache','mustache'),
	(471,'cup2','cup2'),
	(472,'coffee','coffee'),
	(473,'paw','paw'),
	(474,'footprint','footprint'),
	(475,'rocket','rocket'),
	(476,'meter2','meter2'),
	(477,'meter-slow','meter-slow'),
	(478,'meter-fast','meter-fast'),
	(479,'hammer2','hammer2'),
	(480,'balance','balance'),
	(481,'fire','fire'),
	(482,'fire2','fire2'),
	(483,'lab','lab'),
	(484,'atom','atom'),
	(485,'atom2','atom2'),
	(486,'bin','bin'),
	(487,'bin2','bin2'),
	(488,'briefcase','briefcase'),
	(489,'briefcase3','briefcase3'),
	(490,'airplane2','airplane2'),
	(491,'airplane3','airplane3'),
	(492,'airplane4','airplane4'),
	(493,'paperplane','paperplane'),
	(494,'car','car'),
	(495,'steering-wheel','steering-wheel'),
	(496,'car2','car2'),
	(497,'gas','gas'),
	(498,'bus','bus'),
	(499,'truck','truck'),
	(500,'bike','bike'),
	(501,'road','road'),
	(502,'train','train'),
	(503,'train2','train2'),
	(504,'ship','ship'),
	(505,'boat','boat'),
	(506,'chopper','chopper'),
	(507,'cube','cube'),
	(508,'cube2','cube2'),
	(509,'cube3','cube3'),
	(510,'cube4','cube4'),
	(511,'pyramid','pyramid'),
	(512,'pyramid2','pyramid2'),
	(513,'package','package'),
	(514,'puzzle','puzzle'),
	(515,'puzzle2','puzzle2'),
	(516,'puzzle3','puzzle3'),
	(517,'puzzle4','puzzle4'),
	(518,'glasses-3d2','glasses-3d2'),
	(519,'brain','brain'),
	(520,'accessibility','accessibility'),
	(521,'accessibility2','accessibility2'),
	(522,'strategy','strategy'),
	(523,'target','target'),
	(524,'target2','target2'),
	(525,'shield-check','shield-check'),
	(526,'shield-notice','shield-notice'),
	(527,'shield2','shield2'),
	(530,'power2','power2'),
	(531,'power3','power3'),
	(532,'switch','switch'),
	(533,'switch22','switch22'),
	(534,'power-cord','power-cord'),
	(541,'playlist','playlist'),
	(542,'playlist-add','playlist-add'),
	(543,'list-numbered','list-numbered'),
	(544,'list','list'),
	(545,'list2','list2'),
	(546,'more','more'),
	(547,'more2','more2'),
	(548,'grid','grid'),
	(549,'grid2','grid2'),
	(550,'grid3','grid3'),
	(551,'grid4','grid4'),
	(552,'grid52','grid52'),
	(553,'grid6','grid6'),
	(554,'grid7','grid7'),
	(555,'tree5','tree5'),
	(556,'tree6','tree6'),
	(557,'tree7','tree7'),
	(558,'lan','lan'),
	(559,'lan2','lan2'),
	(560,'lan3','lan3'),
	(561,'menu','menu'),
	(562,'circle-small','circle-small'),
	(563,'menu2','menu2');

/*!40000 ALTER TABLE `icons` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table languages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `languages`;

CREATE TABLE `languages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(2) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;

INSERT INTO `languages` (`id`, `code`, `name`, `language`)
VALUES
	(1,'sk','slovak','Slovenčina'),
	(2,'cz','czech','Čeština'),
	(3,'en','english','English');

/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table login_attempts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table navigation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `navigation`;

CREATE TABLE `navigation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `controller` varchar(40) DEFAULT NULL,
  `icon` int(15) DEFAULT NULL,
  `type` varchar(12) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `active` int(1) DEFAULT NULL,
  `deleted` int(1) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `placeholder` varchar(1) DEFAULT NULL,
  `link` int(1) DEFAULT NULL,
  `link_path` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

LOCK TABLES `navigation` WRITE;
/*!40000 ALTER TABLE `navigation` DISABLE KEYS */;

INSERT INTO `navigation` (`id`, `name`, `controller`, `icon`, `type`, `order`, `active`, `deleted`, `parent`, `placeholder`, `link`, `link_path`)
VALUES
	(2,'Dashboard','1',2,'3',1,1,0,0,NULL,NULL,NULL),
	(3,'Developer zone','',145,'2',2,1,0,0,'1',NULL,NULL),
	(4,'CMS Nav','2',545,'3',3,1,0,3,NULL,NULL,NULL),
	(5,'Auth','3',357,'3',2,1,0,3,'',NULL,NULL),
	(6,'Controllers','4',415,'3',4,1,0,3,NULL,NULL,NULL);

/*!40000 ALTER TABLE `navigation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table navigation_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `navigation_type`;

CREATE TABLE `navigation_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `value` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

LOCK TABLES `navigation_type` WRITE;
/*!40000 ALTER TABLE `navigation_type` DISABLE KEYS */;

INSERT INTO `navigation_type` (`id`, `name`, `value`)
VALUES
	(1,'Link','link'),
	(2,'Placeholder','placeholder'),
	(3,'Controller','controller');

/*!40000 ALTER TABLE `navigation_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_permissions`;

CREATE TABLE `user_permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_group_id` int(11) DEFAULT NULL,
  `controller_id` int(11) DEFAULT NULL,
  `show` tinyint(1) DEFAULT '0',
  `create` tinyint(1) DEFAULT '0',
  `edit` tinyint(1) DEFAULT '0',
  `delete` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4;

LOCK TABLES `user_permissions` WRITE;
/*!40000 ALTER TABLE `user_permissions` DISABLE KEYS */;

INSERT INTO `user_permissions` (`id`, `user_group_id`, `controller_id`, `show`, `create`, `edit`, `delete`)
VALUES
	(1,1,1,1,1,1,1),
	(2,1,2,1,1,1,1),
	(3,1,3,1,1,1,1),
	(7,1,4,1,1,1,1);

/*!40000 ALTER TABLE `user_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `lang` varchar(2) DEFAULT 'sk',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_email` (`email`),
  UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  UNIQUE KEY `uc_remember_selector` (`remember_selector`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `salt`, `lang`)
VALUES
	(1,'127.0.0.1','administrator','$2y$08$3crWDvzv7eZtNTMAs5LCJ.8ODdXnIFSTye8VPy4y7Zu0nvv4/AgiG','admin@admin.com',NULL,'',NULL,NULL,NULL,NULL,NULL,1268889823,1552632383,1,'Admin','istrator','ADMIN','0','1','sk');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_groups`;

CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`)
VALUES
	(13,1,1),
	(14,1,2);

/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
