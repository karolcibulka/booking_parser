<?php

$lang['language.sk'] = 'Slovenský';
$lang['language.cz'] = 'Český';
$lang['language.en'] = 'Anglický';

$lang['fields.fax'] = 'Fax';
$lang['fields.phone'] = 'Telefon';
$lang['fields.email'] = 'Email';
$lang['fields.latestNews'] = 'Novinky';
$lang['fields.usefulLinks'] = 'Užitečné linky';
$lang['fields.contactUs'] = 'Kontaktujte nás';
$lang['fields.blog'] = 'Blog';
$lang['fields.showMore'] = 'Zobrazit';
$lang['fields.partners'] = 'Partneri';
$lang['fields.bookNow'] = 'Rezervovať';

$lang['breadcrumb.home'] = 'Domů';

$lang['packages.otherPackages'] = 'Další balíčky v naší nabídce';
$lang['packages.priceInclude'] = 'V ceně';
$lang['packages.description'] = 'Popis balíčku';
$lang['packages.showPackage'] = 'Zobrazit balíček';
$lang['packages.bookNow'] = 'Objednat';

$lang['rooms.otherRooms'] = 'Další pokoje v naší nabídce';
$lang['rooms.priceInclude'] = 'V ceně';
$lang['rooms.description'] = 'Popis pokoje';
$lang['rooms.showRoom'] = 'Zobrazit pokoj';
$lang['rooms.facilities'] = 'Vybavení pokoje';
$lang['rooms.gallery'] = 'Galéria izby';
$lang['rooms.children'] = 'Počet dětí';
$lang['rooms.childAge'] = 'Věk dětí';
$lang['rooms.adults'] = 'Počet dospělých';
$lang['rooms.dateFrom'] = 'Příchod';
$lang['rooms.dateTo'] = 'Odchod';
$lang['rooms.bookOnline'] = 'Objednat pokoj';
$lang['rooms.bookNow'] = 'Objednat';
$lang['rooms.chooseRoom'] = 'Výběr pokoje';

$lang['tables.bookTable'] = 'Rezervovat stoly';

$lang['url.article'] = 'clanek';

$lang['blog.readMore'] = 'Zobrazit článek';
$lang['blog.categories'] = 'Kategorie';
$lang['blog.latestPosts'] = 'Nejnovější příspěvky';
$lang['blog.tags'] = 'Tagy';
$lang['blog.archive'] = 'Archiv';
$lang['blog.search'] = 'Vyhledat';
$lang['blog.searchResults'] = 'Výsledky hledání pro';
$lang['blog.noResults'] = 'Nebyly nalezeny žádné výsledky pro hledaný výraz';

$lang['month.01'] = 'Leden';
$lang['month.02'] = 'Únor';
$lang['month.03'] = 'Březen';
$lang['month.04'] = 'Duben';
$lang['month.05'] = 'Květen';
$lang['month.06'] = 'Červen';
$lang['month.07'] = 'Červenec';
$lang['month.08'] = 'Srpen';
$lang['month.09'] = 'Září';
$lang['month.10'] = 'Říjen';
$lang['month.11'] = 'Listopad';
$lang['month.12'] = 'Prosinec';


$lang['fields.home'] = 'Domov';

$lang['contact.street'] = 'Ulica';
$lang['contact.city'] = 'Město';
$lang['contact.name'] = 'Jméno';
$lang['contact.message'] = 'Správa';
$lang['contact.notification'] = 'Hlásení';
$lang['contact.available'] = 'K dispozici';
$lang['contact.send'] = 'ODESLAŤ';
$lang['contact.about'] = 'O NÁS';

$lang['button.sigin'] = 'ODEBÍRAT';