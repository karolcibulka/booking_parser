<?php

$lang['language.sk'] = 'Slovenský';
$lang['language.cz'] = 'Český';
$lang['language.en'] = 'Anglický';

$lang['fields.fax'] = 'Fax';
$lang['fields.phone'] = 'Telefón';
$lang['fields.email'] = 'Email';
$lang['fields.latestNews'] = 'Novinky';
$lang['fields.usefulLinks'] = 'Užitočné linky';
$lang['fields.contactUs'] = 'Kontaktujte nás';
$lang['fields.blog'] = 'Blog';
$lang['fields.showMore'] = 'Zobraziť';
$lang['fields.partners'] = 'Partneri';
$lang['fields.bookNow'] = 'ONLINE REZERVOVAŤ';

$lang['breadcrumb.home'] = 'Domov';

$lang['packages.otherPackages'] = 'Ďalšie balíčky v našej ponuke';
$lang['packages.priceInclude'] = 'Čo je v cene?';
$lang['packages.description'] = 'Popis balíčka';
$lang['packages.showPackage'] = 'Zobraziť balíček';
$lang['packages.bookNow'] = 'Objednať';

$lang['rooms.otherRooms'] = 'Ďalšie izby v našej ponuke';
$lang['rooms.priceInclude'] = 'V cene';
$lang['rooms.description'] = 'Popis izby';
$lang['rooms.showRoom'] = 'Zobraziť izbu';
$lang['rooms.gallery'] = 'Galéria izby';
$lang['rooms.facilities'] = 'Vybavenie izby';
$lang['rooms.children'] = 'Počet detí';
$lang['rooms.childAge'] = 'Vek detí';
$lang['rooms.adults'] = 'Počet dospelých';
$lang['rooms.dateFrom'] = 'Príchod';
$lang['rooms.dateTo'] = 'Odchod';
$lang['rooms.bookOnline'] = 'Objednať izbu';
$lang['rooms.bookNow'] = 'Objednať';
$lang['rooms.chooseRoom'] = 'Výber izby';

$lang['tables.bookTable'] = 'Rezervovať stôl';

$lang['url.article'] = 'clanok';

$lang['blog.readMore'] = 'Zobraziť článok';
$lang['blog.categories'] = 'Kategórie';
$lang['blog.latestPosts'] = 'Najnovšie príspevky';
$lang['blog.tags'] = 'Tagy';
$lang['blog.archive'] = 'Archív';
$lang['blog.search'] = 'Vyhľadať';
$lang['blog.searchResults'] = 'Výsledky vyhľadávania pre ';
$lang['blog.noResults'] = 'Nenašli sa žiadne výsledky pre hľadaný výraz';

$lang['month.01'] = 'Január';
$lang['month.02'] = 'Február';
$lang['month.03'] = 'Marec';
$lang['month.04'] = 'Apríl';
$lang['month.05'] = 'Máj';
$lang['month.06'] = 'Jún';
$lang['month.07'] = 'Júl';
$lang['month.08'] = 'August';
$lang['month.09'] = 'September';
$lang['month.10'] = 'Október';
$lang['month.11'] = 'November';
$lang['month.12'] = 'December';


$lang['fields.home'] = 'Domov';


$lang['contact.street'] = 'Ulica';
$lang['contact.city'] = 'Mesto';
$lang['contact.name'] = 'Meno';
$lang['contact.message'] = 'Správa';
$lang['contact.notification'] = 'Hlásenie';
$lang['contact.available'] = 'K dispozícii';
$lang['contact.send'] = 'ODOSLAŤ';
$lang['contact.about'] = 'O NÁS';

$lang['button.sigin'] = 'ODOBERAŤ';

$lang['food.order'] = 'Objednať si jedlo';
$lang['food.noFood'] = 'Žiadne dostupné menu pre tento deň!';
$lang['food.main'] = 'Hlavné jedlo';
$lang['food.soup'] = 'Polievka';
$lang['food.desert'] = 'Dezert';
$lang['food.drink'] = 'Nápoj';
$lang['food.0'] = 'Nepálivé';
$lang['food.1'] = 'Jemne pálivé';
$lang['food.2'] = 'Pálivé';

