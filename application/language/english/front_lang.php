<?php

$lang['language.sk'] = 'Slovak';
$lang['language.cz'] = 'Czech';
$lang['language.en'] = 'English';

$lang['fields.fax'] = 'Fax';
$lang['fields.phone'] = 'Phone';
$lang['fields.email'] = 'Email';
$lang['fields.latestNews'] = 'Latest News';
$lang['fields.usefulLinks'] = 'Useful Links';
$lang['fields.contactUs'] = 'Contact Us';
$lang['fields.blog'] = 'Blog';
$lang['fields.showMore'] = 'Show';
$lang['fields.partners'] = 'Partners';
$lang['fields.bookNow'] = 'Book now';

$lang['breadcrumb.home'] = 'Home';

$lang['packages.otherPackages'] = 'Other Packages';
$lang['packages.priceInclude'] = 'Price Include';
$lang['packages.description'] = 'Description';
$lang['packages.showPackage'] = 'Show Package';
$lang['packages.bookNow'] = 'Book Now';

$lang['rooms.otherRooms'] = 'Other Rooms';
$lang['rooms.priceInclude'] = 'Price Include';
$lang['rooms.description'] = 'Description';
$lang['rooms.showRoom'] = 'Show Room';
$lang['rooms.gallery'] = 'Room gallery';
$lang['rooms.facilities'] = 'Facilities';
$lang['rooms.children'] = 'Children';
$lang['rooms.childAge'] = 'Age of Child';
$lang['rooms.adults'] = 'Adults';
$lang['rooms.dateFrom'] = 'Arrival';
$lang['rooms.dateTo'] = 'Departure';
$lang['rooms.bookOnline'] = 'Book Online';
$lang['rooms.bookNow'] = 'Book Now';
$lang['rooms.chooseRoom'] = 'Choose Room';

$lang['tables.bookTable'] = 'Table reservation';

$lang['url.article'] = 'Article';

$lang['blog.readMore'] = 'Read More';
$lang['blog.categories'] = 'Categories';
$lang['blog.latestPosts'] = 'Latest news';
$lang['blog.tags'] = 'Tags';
$lang['blog.archive'] = 'Archive';
$lang['blog.search'] = 'Search';
$lang['blog.searchResults'] = 'Searching results for ';
$lang['blog.noResults'] = 'No results for searching';

$lang['month.01'] = 'January';
$lang['month.02'] = 'February';
$lang['month.03'] = 'March';
$lang['month.04'] = 'April';
$lang['month.05'] = 'May';
$lang['month.06'] = 'June';
$lang['month.07'] = 'July';
$lang['month.08'] = 'August';
$lang['month.09'] = 'September';
$lang['month.10'] = 'October';
$lang['month.11'] = 'November';
$lang['month.12'] = 'December';



$lang['fields.home'] = 'Home';

$lang['contact.street'] = 'Street';
$lang['contact.city'] = 'City';
$lang['contact.name'] = 'Name';
$lang['contact.message'] = 'Message';
$lang['contact.notification'] = 'Notification';
$lang['contact.available'] = 'AVAILABLE';
$lang['contact.send'] = 'SEND NOW';
$lang['contact.about'] = 'ABOUT';

$lang['button.sigin'] = 'SUBSCRIBE';