<?php


class Booking extends API_CONTROLLER
{

	protected $cache_path;
	protected $required_fields;
	protected $parser_config;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Hotel_model','hotel_model');
		$this->cache_path = APPPATH.'/cache/bookingdotcomrooms/';
		$this->required_fields = array('web','destination_id','lang');

		$this->config->load('parser');
		$this->parser_config = $this->config->item('parser');

		//pre_r($this->parser_config);exit;

	}

	public function getParsedHotel_get($hotelID,$month = false,$year = false){
		if(!$hotelID) $this->response(unsuccessResult('HotelID'),200);

		if(!$month){
			$month = date('m');
		}
		if(!$year){
			$year = date('Y');
		}

		$paths = array();

		for( $i = 0 ; $i < $this->parser_config['history_days']; $i++ ){
			$paths[date('Y-m-d',strtotime('- '.$i.' days'))] = APPPATH.'/cache/bookingdotcom/'.str_replace('-','/',date('Y-m-d',strtotime('- '.$i.' days'))).'/';
		}

		$data = array();

		if($hotels = $this->hotel_model->getHotelConnections($hotelID)){
			foreach($hotels as $hotel){
				if($grab = $this->validateHotel($hotel)){
					foreach($paths as $data_from_date => $path){
						$file_path = $path.$grab['destination_id'].'_'.slugify($grab['web']).'.bdc';

						if(file_exists($file_path)){
							$data[$data_from_date][$grab['id']] = json_decode(file_get_contents($path.$grab['destination_id'].'_'.slugify($grab['web']).'.bdc'),true);
						}

					}
				}
			}
		}

		if(!empty($hotels) && !empty($data)){
			if($response = $this->doFinalResponse($hotels,$data,$month,$year)){
				$this->response(successResult($response),200);
			}
		}

		$this->response(unsuccessResult('Neexistujú žiadne dáta'),200);

	}



	public function getRooms_get($id = false){
		if(!$id){
			$this->response(unsuccessResult('Chýba parameter funkcie'), 200);
		}
		if($grab = $this->validateHotel($this->hotel_model->getGrabHotel($id))) {
			if (file_exists($this->cache_path . $grab['destination_id'] .'_'.slugify($grab['web']). '.bdc')) {

				$content = file_get_contents($this->cache_path . $grab['destination_id'].'_'.slugify($grab['web']). '.bdc');
				$this->response(successResult(json_decode($content,true)), 200);


			}
			else{
				$this->response(unsuccessResult('Žiadne dáta'),200);
			}
		}
		else{
			$this->response(unsuccessResult('Hotel neexistuje alebo neaktívny'),200);
		}
	}

	public function synchronizeRooms_get($id = false,$iterationCount = false){
		if(!$id){
			$this->response(unsuccessResult('Chýba parameter funkcie'), 200);
		}

		$count_of_iterations = 3;

		if($iterationCount){
			$count_of_iterations = $iterationCount;
		}


		if($grab = $this->validateHotel($this->hotel_model->getGrabHotel($id))) {

			$this->load->library('BookingParser', false, 'bookingParser');

			$this->bookingParser->setDestinationID($grab['destination_id']);
			$this->bookingParser->setHotelUrl($grab['web']);
			$this->bookingParser->setLang($grab['lang']);
			$this->bookingParser->setRoomsPath($this->cache_path);
			$this->bookingParser->unsetOldRooms();

			for($i = 1;$i <= $count_of_iterations;$i++){
				$random_number = rand(1,180);
				$this->bookingParser->setCheckin(date('Y-m-d',strtotime('+ '.$random_number.' days')));
				$this->bookingParser->setCheckout(date('Y-m-d',strtotime('+ '.($random_number+1).' days')));
				for($j = 1 ; $j <= 3;$j++){
					$this->bookingParser->setAdults($j);
					$this->bookingParser->getHotelRooms();
				}

			}

			if (file_exists($this->cache_path . $grab['destination_id'].'_'.slugify($grab['web']) . '.bdc')) {
				$content = file_get_contents($this->cache_path . $grab['destination_id'].'_'.slugify($grab['web']) . '.bdc');
				$this->response(successResult(json_decode($content,true)), 200);
			}
			else{
				$this->response(unsuccessResult('Nie sú dostupné žiadne hotely'),200);
			}

		}
		else{
			$this->response(unsuccessResult('Daný hotel neexistuje/nie je aktívny'), 200);
		}
	}


	private function validateHotel($hotel){
		$state = true;
		foreach($this->required_fields as $required_field){
			if(!isset($hotel[$required_field]) || empty($hotel[$required_field])){
				$state = false;
				break;
			}
		}

		if($state){
			return $hotel;
		}

		return array();
	}

	private function doFinalResponse($hotels,$data,$month,$year){

		$response = array();

		$today = date('Y-m-d');
		$yesterday = date('Y-m-d',strtotime('- 1 day'));

		foreach($data as $create_date => $data_item){
			foreach($hotels as $hotelID => $hotel){
				if(isset($data_item[$hotelID]) && !empty($data_item[$hotelID])){
					foreach($data_item[$hotelID] as $dateFromTo => $item){
						$date = explode('_',$dateFromTo);
						if($this->validateDate($year,$month,$date[0])){
							foreach($item as $room){

								if(isset($room['room_type_id'],$hotel['rooms'][$room['room_type_id']]) && !empty($hotel['rooms'][$room['room_type_id']])){

									$response[$room['room_type_id']][$room['room_id']]['hotel_name'] = $hotel['name'];
									$response[$room['room_type_id']][$room['room_id']]['room_name'] = $room['room_name'];


									if(isset($room['room_items']) && !empty($room['room_items'])){

										foreach($room['room_items'] as $person_count => $room_item){
											if(strtotime($today) === strtotime($create_date)){
												$response[$room['room_type_id']][$room['room_id']]['persons'][$person_count][$date[0]]['today_price'] = $room_item['room_price'];
											}
											if(strtotime($yesterday) === strtotime($create_date)){
												$response[$room['room_type_id']][$room['room_id']]['persons'][$person_count][$date[0]]['yesterday_price'] = $room_item['room_price'];
											}

											$response[$room['room_type_id']][$room['room_id']]['persons'][$person_count][$date[0]]['all_prices'][$create_date] = $room_item['room_price'];
										}

									}
								}
							}
						}
					}
				}
			}
		}



		if(!empty($response)){
			foreach($response as $response_key => $response_item){
				if(!empty($response_item)){
					foreach($response_item as $room_id =>  $room){
						if(isset($room['persons']) && !empty($room['persons'])){
							foreach($room['persons'] as $person_count => $item){
								$response[$response_key][$room_id]['persons'][$person_count] = $this->checkAllFields($item,$month,$year);
							}
						}
					}
				}
			}
		}

		return $response;
	}

	private function checkAllFields($items,$month,$year){

		$firstDay = strtotime(date('Y-m-d',strtotime($year.'-'.$month.'-01')));
		$lastDay = strtotime(date('Y-m-t',$firstDay));

		while($firstDay <= $lastDay){
			$date = date('Y-m-d',$firstDay);

			if(isset($items[$date]) && !empty($items[$date])){
				if(!isset($items[$date]['today_price']) || empty($items[$date]['today_price'])){
					$items[$date]['today_price'] = '!';
				}
				if(!isset($items[$date]['yesterday_price']) || empty($items[$date]['yesterday_price'])){
					$items[$date]['yesterday_price'] = '!';
				}
				if(!isset($items[$date]['all_prices']) || empty($items[$date]['all_prices'])){
					$items[$date]['all_prices'] = $this->checkAllDaysFields(array());
				}
				else{
					$items[$date]['all_prices'] = $this->checkAllDaysFields($items[$date]['all_prices']);
				}
			}
			else{
				$items[$date] = array(
					'today_price' => '!',
					'yesterday_price' => '!',
					'all_prices' => $this->checkAllDaysFields(array())
				);
			}


			$firstDay = strtotime(date('Y-m-d',strtotime('+ 1 day',$firstDay)));
		}

		ksort($items);

		return $items;

	}

	private function checkAllDaysFields($items = array()) {
		$today = strtotime(date('Y-m-d'));
		$history_to = strtotime(date('Y-m-d',strtotime('- '.$this->parser_config['history_days'].' days',$today)));

		while($history_to < $today){
			$date = date('Y-m-d',$today);

			if(!isset($items[$date]) || empty($items[$date])){
				$items[$date] = '!';
			}

			$today = strtotime(date('Y-m-d',strtotime('- 1 day',$today)));

		}

		ksort($items);

		return $items;

	}

	private function validateDate($year,$month,$date){
		$firstDay = strtotime(date('Y-m-d',strtotime($year.'-'.$month.'-01')));
		$lastDay = strtotime(date('Y-m-t',$firstDay));

		if($firstDay <= strtotime($date) && $lastDay >= strtotime($date)){
			return true;
		}

		return false;
	}
}
