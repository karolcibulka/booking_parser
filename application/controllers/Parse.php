<?php


class Parse extends MY_Controller
{

	protected $max_adults_iteration;
	protected $required_fields;
	protected $parser_config;

	public function __construct()
	{
		parent::__construct();

		$this->load->library('BookingParser',false,'bookingParser');
		$this->load->model('Hotel_model','hotel_model');

		$this->max_adults_iteration = 3;
		$this->required_fields = array('web','destination_id','lang');

		$this->config->load('parser');
		$this->parser_config = $this->config->item('parser');
	}

	public function index(){

		$this->parse();
	}

	private function parse(){
		if($hotels = $this->hotel_model->getGrabHotels()){
			foreach($hotels as $hotel) {
				if($grab = $this->validateHotel($hotel)){
					$this->bookingParser->setLang($grab['lang']);
					$this->bookingParser->setHotelUrl($grab['web']);
					$this->bookingParser->setDestinationID($grab['destination_id']);
					for( $i = 0 ; $i<$this->parser_config['grab_days'] ; $i++ ){
						$this->bookingParser->setCheckin(date('Y-m-d',strtotime('+ '.$i.'days')));
						$this->bookingParser->setCheckout(date('Y-m-d',strtotime('+ '.($i+1).'days')));
						for ($j = 1; $j <= $this->max_adults_iteration; $j++) {
							$this->bookingParser->setAdults($j);
							$this->bookingParser->parseSite();
						}
					}
				}
			}
		}
	}

	public function parseHotel($id){
		if($grab = $this->validateHotel($this->hotel_model->getGrabHotel($id))){
			$this->bookingParser->setLang($grab['lang']);
			$this->bookingParser->setHotelUrl($grab['web']);
			$this->bookingParser->setDestinationID($grab['destination_id']);
			for( $i = 0 ; $i<$this->parser_config['grab_days'] ; $i++ ){
				$this->bookingParser->setCheckin(date('Y-m-d',strtotime('+ '.$i.'days')));
				$this->bookingParser->setCheckout(date('Y-m-d',strtotime('+ '.($i+1).'days')));
				for ($j = 1; $j <= $this->max_adults_iteration; $j++) {
					$this->bookingParser->setAdults($j);
					$this->bookingParser->parseSite();
				}
			}
		}
		else{
			exit('nezvalidované');
		}
	}

	public function getHotelData($id){
		if($grab = $this->validateHotel($this->hotel_model->getGrabHotel($id))){
			$path_to_file = APPPATH.'cache/bookingdotcom/'.date('Y-m-d').'/'.$grab['destination_id'].'_'.slugify($grab['web']).'.bdc';
			if(file_exists($path_to_file)){
				pre_r(json_decode(file_get_contents($path_to_file),true));exit;
			}
		}
		else{
			exit('nezvalidované');
		}
	}

	private function validateHotel($hotel){
		$state = true;
		foreach($this->required_fields as $required_field){
			if(!isset($hotel[$required_field]) || empty($hotel[$required_field])){
				$state = false;
				break;
			}
		}

		if($state){
			return $hotel;
		}

		return array();
	}
}
