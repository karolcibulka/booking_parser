<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['theme']['version'] = 'default2'; //default/default2/restart/hrebienok /granithotels //bystra
$config['theme']['hotel_id'] = '12';
$config['theme']['redirect'] = 'https://12.booking.softsolutions.sk/';

$config['theme']['facebook_id'] = '';
$config['theme']['facebook_logged_in_greeting'] = 'Dobrý deň, ako Vám môžeme pomôcť?';
$config['theme']['facebook_logged_out_greeting'] = 'Dobrý deň, ako Vám môžeme pomôcť?';
$config['theme']['facebook_theme_color'] = '#0084ff';