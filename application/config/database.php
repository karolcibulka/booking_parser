<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$active_group = 'default';
$query_builder = TRUE;


$server = 'master1.softsolutions.sk:13306';
$username = 'user@hoteladmin';
$pass = 'ENeQJcdrA2Rb5ZGq';
$database = 'hoteladmin';

$db['default'] = array(
    'dsn'	=> '',
    'hostname' => $server,
    'username' => $username,
    'password' => $pass,
    'database' => $database,
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'production'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);
