<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *  Template library
 */
class Template
{
	var $ci;



	/**
	 *  Get Codeigniter instance
	 */
	function __construct()
	{
		$this->ci =& get_instance();
	}

	/**
	 * Method for template loading
	 *
	 * @param $tpl_view
	 * @param null $body_view
	 * @param null $data
	 */

	private $layout_data = array();

	function set($name, $value)
	{
		$this->layout_data[$name] = $value;
	}


	function load($tpl_view, $body_view = null, $data = null)
	{
		$this->set('body', $this->ci->load->view($body_view, $data, TRUE));

		$this->ci->load->view('layout/'.$tpl_view, $this->layout_data);
	}

	function view($view = '', $view_data = array())
	{

		$this->set('content', $this->ci->load->view($this->theme . '/views/' . $view, array_merge($view_data, $this->layout_data) , TRUE));

		return $this->ci->load->view($this->theme .'/'. $this->layout, $this->layout_data);
	}

	function getView($view = '', $view_data = array())
	{
		return $this->ci->load->view($view, $view_data, TRUE);
	}






}
