<?php


class BookingParser
{

	protected $ci;
	protected $url;

	protected $lang;
	protected $hotelUrl;
	protected $params;

	protected $checkin;
	protected $checkout;
	protected $dest_id;
	protected $adults = '2';
	protected $children = '0';

	protected $cookie_jar;
	protected $cache_path;
	protected $roomsPath;
	protected $user_agent;

	protected $response;

	protected $items;
	protected $store_key;

	protected $base_path = 'https://www.booking.com/hotel/';


	public function __construct($cookieJarFile = 'cookies.txt')
	{
		$this->ci =  get_instance();

		$this->cookie_jar = strtotime(date('Y-m-d H:i:s')).'_'.$cookieJarFile;
		$this->user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US; rv:1.8.1.7) Gecko/20070914 Firefox/2.0.0.7';


		$this->cache_path = APPPATH.'cache/bookingdotcom/'.date('Y').'/'.date('m').'/'.date('d');

		if(!file_exists($this->cache_path)){
			mkdir($this->cache_path,0777,true);
		}
	}

	public function setLang($lang){
		$this->lang = $lang;
	}

	public function clearResponse(){
		$this->response = array();
	}

	public function setHotelUrl($url){
		$this->hotelUrl = $url;
	}

	public function setRoomsPath($path){
		if(!file_exists($path)){
			mkdir($path,0777,true);
		}

		$this->roomsPath = $path;
	}

	public function setCheckin($checkin){
		$this->checkin = date('Y-m-d',strtotime($checkin));
	}

	public function setCheckout($checkout){
		$this->checkout = date('Y-m-d',strtotime($checkout));
	}

	public function setDestinationID($destination_id){
		$this->dest_id = $destination_id;
	}

	public function setAdults($adults){
		$this->adults = $adults;
	}

	public function setChildren($children){
		$this->children = $children;
	}

	public function getSearchParams(){
		$this->params = array(
			'checkin' => $this->checkin,
			'checkout' => $this->checkout,
			'dest_id' => $this->dest_id,
			'dest_type' => 'city',
			'group_adults' => $this->adults,
			'group_children' => $this->children,
		);

		return http_build_query($this->params);
	}

	public function unsetOldRooms(){
		if(file_exists($this->roomsPath.'/'.$this->dest_id.'_'.slugify($this->hotelUrl).'.bdc')){
			unlink($this->roomsPath.'/'.$this->dest_id.'_'.slugify($this->hotelUrl).'.bdc');
		}
	}

	public function setUrl(){
		$this->url = $this->base_path.$this->lang.'/'.$this->hotelUrl.'.html?'.$this->getSearchParams();
	}

	public function parseSite(){
		$this->clearResponse();
		$this->setUrl();
		$html = $this->get($this->url);

		$this->response = $this->getResponse($html);

		$this->storeLogData();
	}

	public function getHotelRooms(){
		$this->clearResponse();
		$this->setUrl();

		$html = $this->get($this->url);

		$this->response = $this->getResponse($html);

		$this->storeLogRoomsData();
	}

	private function storeLogRoomsData(){

		$this->items = json_encode(array());

		$bdc_file = $this->roomsPath.'/'.$this->dest_id.'_'.slugify($this->hotelUrl).'.bdc';

		if(file_exists($bdc_file)){
			$this->items = file_get_contents($bdc_file);
		}
		else{
			$my_file = fopen($bdc_file, 'a+') or die("Unable to open file!");
			fwrite($my_file, '');
			fclose($my_file);
		}

		$this->items = json_decode($this->items,TRUE);

		file_put_contents($bdc_file,json_encode($this->prettify_rooms_response()));

	}

	private function storeLogData(){
		$this->items = json_encode(array());

		$bdc_file = $this->cache_path.'/'.$this->dest_id.'_'.slugify($this->hotelUrl).'.bdc';

		if(file_exists($bdc_file)){
			$this->items = file_get_contents($bdc_file);
		}
		else{
			$my_file = fopen($bdc_file, 'a+') or die("Unable to open file!");
			fwrite($my_file, '');
			fclose($my_file);
		}

		$this->items = json_decode($this->items,TRUE);

		$this->store_key = $this->checkin.'_'.$this->checkout;

		$this->items[$this->store_key] = $this->prettify_response();

		file_put_contents($bdc_file,json_encode($this->items));

	}

	private function prettify_rooms_response(){

		if(isset($this->response) && !empty($this->response)) {
			foreach ($this->response as $key => $res) {
				//if (isset($res['b_blocks']) && !empty($res['b_blocks'])) {
					if(!isset($this->items[$res['b_id']]) || empty($this->items[$res['b_id']])){
						$this->items[$res['b_id']] = array(
							'room_name' => $res['b_name'],
							'room_id' => $res['b_id'],
							'room_type_id' => $res['b_roomtype_id'],
							'room_type' => $this->getRoomType($res['b_roomtype_id']),
						);
					}
				//}
			}
		}
		return $this->items;

	}

	private function prettify_response(){
		$response = array();

		$returnError = false;
		if(!empty($this->response)){
			foreach($this->response as $key => $res){
				if(isset($res['b_blocks']) && !empty($res['b_blocks'])){
					foreach($res['b_blocks'] as $block_id => $block){
						$response[$res['b_id']]['room_name'] = $res['b_name'];
						$response[$res['b_id']]['room_id'] = $res['b_id'];
						$response[$res['b_id']]['room_type_id'] = $res['b_roomtype_id'];
						$response[$res['b_id']]['room_type'] = $this->getRoomType($res['b_roomtype_id']);


						$response[$res['b_id']]['room_items'][$block['b_max_persons']]['room_price'] = $block['b_raw_price'];

						if(isset($this->items[$this->store_key][$res['b_id']]['room_items'][$block['b_max_persons']]) && !empty($this->items[$this->store_key][$res['b_id']]['room_items'][$block['b_max_persons']])){
							$oldPrice = $this->items[$this->store_key][$res['b_id']]['room_items'][$block['b_max_persons']]['room_price'];
							$newPrice = $block['b_raw_price'];

							if($this->formatPrice($oldPrice) < $this->formatPrice($newPrice)){
								$response[$res['b_id']]['room_items'][$block['b_max_persons']]['room_price'] = $this->items[$this->store_key][$res['b_id']]['room_items'][$block['b_max_persons']]['room_price'];
							}
						}

					}
				}
			}
		}
		else{
			if(!isset($this->items[$this->store_key]) || empty($this->items[$this->store_key])){
				$response['error'] = 'Toto ubytovanie je na našej webovej stránke od '.$this->checkin.' do '.$this->checkout.' plne obsadené.';
			}
		}

		return $response;
	}

	private function formatPrice($price){
		return number_format($price,'2','.','');
	}

	private function getResponse($html){

		$html  = str_replace('<!DOCTYPE html>',"<!DOCTYPE html>\n<html>",$html);

		$dom = new DomDocument();
		@ $dom->loadHTML($html);

		$scripts = $dom->getElementsByTagName('script');

		$response = array();

		foreach($scripts as $script) {
			if (strpos($script->textContent, 'b_rooms_available_and_soldout') !== false) {
				$response = $this->parseJavascriptToResponse($script->textContent);
			}
		}

		//pre_r($response);exit;

		return $response;
	}



	private function parseJavascriptToResponse($html){
		$str = explode('b_photo_pid: (1).toString(),',$html);
		$str = explode('b_gallery_has_photo_tags: false,',$str[0]);
		$str = str_replace('b_rooms_available_and_soldout: ','',$str[1]);
		$str = substr($str, 0, -1);
		$str = substr($str, 0, -1);

		return json_decode($str,true);
	}

	private function setup(){

		$header = array(
			'Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5',
			'Cache-Control: max-age=0',
			'Connection: keep-alive',
			'Keep-Alive: 300',
			'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7',
			'Accept-Language: en-us,en;q=0.5',
			'Pragma: '
		);


		curl_setopt($this->curl, CURLOPT_USERAGENT, $this->user_agent);
		curl_setopt($this->curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt($this->curl,CURLOPT_COOKIEJAR, $this->cookie_jar);
		curl_setopt($this->curl,CURLOPT_COOKIEFILE, $this->cookie_jar);
		curl_setopt($this->curl,CURLOPT_AUTOREFERER, true);
		curl_setopt($this->curl,CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($this->curl,CURLOPT_RETURNTRANSFER, true);
	}


	private function get($url){
		$this->curl = curl_init($url);
		$this->setup();

		return $this->request();
	}

	private function getAll($reg,$str){
		preg_match_all($reg,$str,$matches);
		return $matches[1];
	}

	private function postForm($url, $fields, $referer=''){
		$this->curl = curl_init($url);
		$this->setup();
		curl_setopt($this->curl, CURLOPT_URL, $url);
		curl_setopt($this->curl, CURLOPT_POST, 1);
		curl_setopt($this->curl, CURLOPT_REFERER, $referer);
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, $fields);
		return $this->request();
	}

	private function getInfo($info)
	{
		$info = ($info == 'lasturl') ? curl_getinfo($this->curl, CURLINFO_EFFECTIVE_URL) : curl_getinfo($this->curl, $info);
		return $info;
	}

	private function request()
	{
		return curl_exec($this->curl);
	}

	private function getRoomType($roomTypeID){
		$room_types = array(
			'10' => 'Single',
			'9' => 'Double',
			'8' => 'Twin',
			'24' => 'Twin/Double',
			'7' => 'Triple',
			'4' => 'Quad',
			'13' => 'Family',
			'5' => 'Suite',
			'12' => 'Studio',
			'1' => 'Apartment',
			'25' => 'Dorm Room',
			'26' => 'Bed in Dorm Room',
			'27' => 'Bungalow',
			'28' => 'Chalet',
			'31' => 'Villa',
			'29' => 'Vacation Home',
			'32' => 'Trailer',
			'33' => 'Tent',
		);

		if(isset($room_types[$roomTypeID]) && !empty($room_types[$roomTypeID])){
			return $room_types[$roomTypeID];
		}

		return '';
	}
}
