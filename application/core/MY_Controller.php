<?php defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class MY_Controller extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->helper(array('app'));

    }
}

class API_CONTROLLER extends REST_Controller {

	public function __construct()
	{
		parent::__construct();

		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: API-KEY, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
		header("Cache-Control: no-cache, must-revalidate");
		$method = $_SERVER['REQUEST_METHOD'];

		if($method == "OPTIONS") {
			die();
		}

		$this->load->helper(array('app'));
		$api_key  = 'tr@veldata';

		if ($apiKey = $this->input->get_request_header('Api-Key')) {

			if ($api_key !== $apiKey) {
				$this->response(array('error' => 403, "Message" => 'Not authorized'), 403);
			}

		}
		else {
			$this->response(array('error' => 403, "Message" => 'Not authorized'), 403);
		}
	}
}
