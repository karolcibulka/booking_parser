<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function has_permission($method = false, $controllerW = false) {
    if(isset($method) && !empty($method)){
        $CI = & get_instance();
        //pre_r($CI->session->userdata());exit;
        if($CI->session->has_userdata('permissions')) {
            $userData = $CI->session->userdata('permissions');

            if(isset($controllerW) && !empty($controllerW)){
                if (isset($userData[$controllerW][$method])) {
                    return (bool)$userData[$controllerW][$method];
                }
            }
            else{
                if (isset($CI->uri->segments[3]) && !empty($CI->uri->segments[3])) {
                    $controller = $CI->uri->segments[3];
                    if (isset($userData[$controller][$method])) {
                        return (bool)$userData[$controller][$method];
                    }
                }
            }
        }
    }
}

