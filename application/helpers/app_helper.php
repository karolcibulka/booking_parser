<?php


function pre_r($expression, $return = FALSE)
{
    list($callee) = debug_backtrace();

    if ($return === true)
    {
    if (is_string($expression)) return '<pre>' . print_r(str_replace(array('<','>'), array('&lt;','&gt;'), $expression), TRUE) . '</pre>';
    return '<pre>' . print_r($expression, TRUE) . '</pre>';
    }
    elseif ($return == 'noDebug')
    {
    echo '<pre>';
        if (is_string($expression)) print_r(str_replace(array('<','>'), array('&lt;','&gt;'), $expression), FALSE);
        else print_r($expression, FALSE);
        echo '</pre>';
    }
    else
    {
    echo  '<strong>'.$callee['file'].'</strong>  <span style="color:tomato">@line '.$callee['line']. '</span>';
    echo '<pre>';
        if (is_string($expression)) print_r(str_replace(array('<','>'), array('&lt;','&gt;'), $expression), FALSE);
        else print_r($expression, FALSE);
        echo '</pre>';
    }
}

function dashboard_base_url($uri = '', $protocol = NULL)
{
    $lang = get_instance()->config->item('language_abbr');
    return get_instance()->config->base_url($lang.'/'.$uri, $protocol);
}

function getLayouts(){
    $ci =& get_instance();
    $ci->load->config('theme');
    $version  = $ci->config->item('theme')['version'];

    $files = scandir(APPPATH.'/views/themes/'.$version.'/layouts');

   $args = array();
    if(!empty($files)){
        foreach($files as $file){
            if($file!='.' && $file!='..'){
                $args[] = str_replace ('.php' ,'', $file );
            }
        }
    }

    return $args;
}

function getBannerItems() {
    $ci = & get_instance();

    $ci->load->config('theme');
    $version  = $ci->config->item('theme')['version'];

    $files = scandir(APPPATH.'/views/themes/'.$version);
    $php_files = array();
    $response = array();

    if(isset($files) && !empty($files)){
        foreach($files as $file){
            if(strpos($file,'php')!==false){
                $php_files[] = $file;
            }
        }
    }

    $non_actepted = ['home','blog','subpage'];

    $response[] = 'own';

    if(!empty($php_files)){
        foreach($php_files as $php_file){
            $php_file_name = str_replace('.php','',$php_file);
            if(!in_array($php_file_name,$non_actepted)){
                $response[] = $php_file_name;
            }
        }
    }

   return $response;
}

function getBanners(){
    $ci =& get_instance();
    $ci->load->config('theme');
    $version  = $ci->config->item('theme')['version'];

    $files = scandir(APPPATH.'/views/themes/'.$version.'/banners');

    $args = array();
    if(!empty($files)){
        foreach($files as $file){
            if($file!='.' && $file!='..'){
                $args[] = str_replace ('.php' ,'', $file );
            }
        }
    }

    return $args;
}

function absolute_asset_url($path){
    return FCPATH.'assets/'.$path;
}


function hasTemplateBlog(){
    $ci =& get_instance();
    $ci->load->config('theme');
    $version  = $ci->config->item('theme')['version'];

    $files = scandir(APPPATH.'/views/themes/'.$version);

    if(in_array('blog.php',$files)){
        return true;
    }

    return false;
}

function hasAllowedMultisite(){
    $ci = & get_instance();
    $ci->load->driver('cache',
        array('adapter' => 'apc', 'backup' => 'file')
    );
    if($settings = $ci->cache->get('property_settings')){
        if(isset($settings['allow_multisite']) && !empty($settings['allow_multisite'])){
            return true;
        }
    }

    return false;
}

function getAllowedMultisites(){
    $ci = & get_instance();

    $ci->load->driver('cache',
        array('adapter' => 'apc', 'backup' => 'file')
    );

    if($multisites = $ci->cache->get('multisites')){
        return $multisites;
    }

    return array();
}

function choiceCurrentLang(){
    $ci = & get_instance();
    if(strlen($ci->uri->segment(1)) == 2){
        return $ci->uri->segment(1);
    }

    return $ci->uri->segment(2);
}

function hasActiveMultisite(){
    $ci = & get_instance();
    if(strlen($ci->uri->segment(1)) == 2){
        return false;
    }

    return true;
}

function getActiveMultisite($slug){
    $multisites = getAllowedMultisites();
    if(!empty($multisites)){
        foreach($multisites as $multisite){
            if($multisite['slug'] == $slug){
                return $multisite;
            }
        }
    }

    return null;
}

function getActiveMultisiteID($slug){
    if(!hasAllowedMultisite()){
        return null;
    }

    $multisites = getAllowedMultisites();
    if(!empty($multisites)){
        foreach($multisites as $multisite){
            if($multisite['slug'] == $slug){
                return $multisite['id'];
            }
        }
    }

    return null;
}

function getMultisiteNameByID($id)
{
    if (!hasAllowedMultisite()) {
        return '';
    }

    $multisites = getAllowedMultisites();
    if (!empty($multisites)) {
        foreach ($multisites as $multisite) {
            if ($multisite['id'] == $id) {
                return $multisite['name'];
            }
        }
    }

    return '';
}



function getMultisiteSlugByID($id){
    if(!hasAllowedMultisite()){
        return '';
    }

    $multisites = getAllowedMultisites();
    if(!empty($multisites)){
        foreach($multisites as $multisite){
            if($multisite['id'] == $id){
                return $multisite['slug'].'/';
            }
        }
    }

    return '';
}

function color_luminance( $hex, $percent ) {

    // validate hex string

    $hex = preg_replace( '/[^0-9a-f]/i', '', $hex );
    $new_hex = '#';

    if ( strlen( $hex ) < 6 ) {
        $hex = $hex[0] + $hex[0] + $hex[1] + $hex[1] + $hex[2] + $hex[2];
    }

    // convert to decimal and change luminosity
    for ($i = 0; $i < 3; $i++) {
        $dec = hexdec( substr( $hex, $i*2, 2 ) );
        $dec = min( max( 0, $dec + $dec * $percent ), 255 );
        $new_hex .= str_pad( dechex( $dec ) , 2, 0, STR_PAD_LEFT );
    }

    return $new_hex;
}


function generateToken($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function getLanguages(){
    return array(
        'sk' => 'slovak',
        'cz' => 'czech',
        'en' => 'english'
    );
}

function getLang($langCode){
    $lang = '';
    switch($langCode){
        case "sk":
            $lang = 'slovak';
            break;
        case "cz":
            $lang = 'czech';
            break;
        case "en":
            $lang = 'english';
            break;
    }

    return $lang;
}

function errorMessage($code, $paramName = NULL)
{
	$message = array('status' => FALSE, 'error' => array());

	switch ($code) {
		case 1001:
			$message['error'] = array(
				'code' => $code,
				'message' => 'Missing param ' . $paramName
			);
			break;
		case 1002:
			$message['error'] = array(
				'code' => $code,
				'message' => $paramName . ' does not exist'
			);
			break;
		case 1003:
			$message['error'] = array(
				'code' => $code,
				'message' => 'Wrong parameter value: ' . $paramName
			);
			break;
		case 1004:
			$message['error'] = array(
				'code' => $code,
				'message' => 'Offer price has changed'
			);
			break;
		case 1005:
			$message['error'] = array(
				'code' => $code,
				'message' => 'Reservation token has expired'
			);
			break;
		case 2001:
			$message['error'] = array(
				'code' => $code,
				'message' => 'Invalid or Missing config in HotelAdmin module: ' . $paramName
			);
			break;
		case 2002:
			$message['error'] = array(
				'code' => $code,
				'message' => 'Invalid or Missing access to price list'
			);
			break;
		case 3001:
			$message['error'] = array(
				'code' => $code,
				'message' => 'Hotel server not responding (connection error)'
			);
			break;
		case 4001:
			$message['error'] = array(
				'code' => $code,
				'message' => 'Not authorized'
			);
			break;
		case 4002:
			$message['error'] = array(
				'code' => $code,
				'message' => 'Unauthorized access to hotel: ' . $paramName
			);
			break;
		case 4003:
			$message['error'] = array(
				'code' => $code,
				'message' => 'Unauthorized access to method: ' . $paramName
			);
			break;
		case 4004:
			$message['error'] = array(
				'code' => $code,
				'message' => 'Unauthorized access to Client: ' . $paramName
			);
			break;
		case 4005:
			$message['error'] = array(
				'code' => $code,
				'message' => 'Unauthorized access to Channel: ' . $paramName
			);
			break;
		case 4006:
			$message['error'] = array(
				'code' => $code,
				'message' => 'Duplicity ClientID: ' . $paramName
			);
			break;

		default:
			$message['error'] = array(
				'code' => 5000,
				'message' => 'Internal server error'
			);
			break;
	}

	return $message;
}

function successResult($data = FALSE)
{
	if ($data)
	{
		return array(
			'status' => TRUE,
			'data' => $data
		);
	}
	else
	{
		return array('status' => TRUE);
	}
}

function successResultWithWarning($message, $data = FALSE)
{
	if ($data)
	{
		return array(
			'status' => TRUE,
			'message' => $message,
			'data' => $data
		);
	}
	else
	{
		return array(
			'status' => TRUE,
			'message' => $message
		);
	}
}

function unsuccessResult($message, $data = FALSE, $code = FALSE)
{

	if (!empty($data))
	{
		if($code != FALSE){
			return array(
				'status' => FALSE,
				'message' => $message,
				'data' => $data,
				'code' => $code,
			);
		}else{
			return array(
				'status' => FALSE,
				'message' => $message,
				'data' => $data
			);
		}

	}
	else
	{
		if($code != FALSE){
			return array(
				'status' => FALSE,
				'message' => $message,
				'code' => $code,
			);
		}else {
			return array(
				'status' => FALSE,
				'message' => $message
			);
		}
	}

}


function slugify($string){
	$ci = & get_instance();
	$ci->load->helper('text');
	return url_title(convert_accented_characters($string), 'dash', true);
}
